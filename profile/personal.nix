{ config, pkgs, ... }:
{
  imports = [
    # Global
    # ------
    ../config/per-host/web.internal.nyarla.net.nix
    ../config/per-location/jp.nix

    # Console
    # =======
    ../config/per-account/console.nix
    ../config/per-bundle/console.nix
    ../config/per-bundle/toolchain.nix
    ../config/per-service/sshd-mosh.nix

    # Hardware
    # ========
    ../config/per-hardware/qemu-bios.nix

    # Services
    # ========
    ../config/per-account/www-data.nix
    ../config/per-service/fathom.nix
    ../config/per-service/h2p.nix
  ];
}
