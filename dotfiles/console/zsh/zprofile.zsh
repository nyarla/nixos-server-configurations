#!/usr/bin/env zsh

CWD=$(pwd)

function has() {
 type ${1:-} 2>&1 >/dev/null
}

if has wcwidth-cjk && has wcwidth ; then
  if test "$(wcwidth '○' |  cut -d' ' -f1)" = "1"; then
    exec wcwidth-cjk -- $SHELL --login
  fi
fi

if has keychain && test -e "$HOME"/.ssh/id_ed25519 ; then
  keychain --nogui --quiet "$HOME"/.ssh/id_ed25519
  source "$HOME"/.keychain/$(hostname)-sh
fi

# TODO: npm
# TODO: cpanm

if test -d /etc/nixos/dotfiles/console ; then
  export DOTFILES=/etc/nixos/dotfiles/console
else
  export DOTFILES="$HOME"/local/dotfiles/dotfiles/console
fi

test -e "$HOME"/.gitignore       || ln -sf "$DOTFILES"/git/gitignore "$HOME"/.gitignore
test -e "$HOME"/.gitconfig       || ln -sf "$DOTFILES"/git/gitconfig "$HOME"/.gitconfig
test -e "$HOME"/.vim.d           || mkdir -p "$HOME"/.vim.d/{swap,backup}
test -e "$HOME"/.config/vim-volt || ln -sf "$DOTFILES"/volt "$HOME"/.config/vim-volt

test -d "$HOME"/.config/zsh      || mkdir -p "$HOME"/.config/zsh
cd "$HOME"/.config/zsh

test -e enhancd || git clone https://github.com/b4b4r07/enhancd
test -e pure    || git clone https://github.com/sindresorhus/pure

test -d "$HOME"/local/zfunctions       || mkdir -p "$HOME"/local/zfunctions
test -e "$HOME"/local/zfunctions/async || ln -sf "$HOME"/.config/zsh/pure/async.zsh "$HOME"/local/zfunctions/async
test -e "$HOME"/local/zfunction/prompt_pure_setup || ln -sf "$HOME"/.config/zsh/pure/pure.zsh "$HOME"/local/zfunctions/prompt_pure_setup

cd $CWD
unset CWD
unset DOTFILES
unset -f has
