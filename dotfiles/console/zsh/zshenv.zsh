#!/usr/bin/evn zsh

function has() {
  type "${1:-}" 2>&1 >/dev/null
}

if has perl && test -d "$HOME"/.config/perl5; then
  eval "$(perl -I"$HOME"/.config/perl5/lib/perl5 -Mlocal::lib="$HOME"/.config/perl5)"
  export PERL_CPANM_OPT="--local-lib-contained ${HOME}/.config/perl5"
fi

export GOPATH="$HOME"/dev
! test -d "$GOPATH"/bin            || export PATH="$GOPATH"/bin:$PATH
! test -d "$HOME"/.config/npm/bin  || export PATH="$HOME"/.config/npm/bin:$PATH
! test -d "$HOME"/.cargo/env       || source "$HOME"/.cargo/env
! test -d "$HOME"/.config/vim-volt || export VOLTPATH="$HOME"/.config/vim-volt
! test -d "$HOME"/local/bin        || export PATH="$HOME"/local/bin:$PATH

if has vim ; then
  export EDITOR=vim
fi

path=(${^path}(N-/^W))
unset -f has
