#!/usr/bin/env -i zsh

export PATH=/run/current-system/sw/bin:$PATH

set -e -u -o pipefail

. /etc/credentials/acme.sh

function issue() {
  local domain=${1}

  if ! test -d /root/.acme.sh ; then
    acme.sh --issue --dns dns_aws -d "${domain}" -d "*.${domain}"
  else
    acme.sh --renew --force --dns dns_aws -d "${domain}" -d "*.${domain}" 
  fi

  test -d /data/cert/${domain} || mkdir -p /data/cert/${domain}

  acme.sh --install-cert -d "${domain}" \
    --key-file /data/cert/${domain}/key.pem \
    --fullchain-file /data/cert/${domain}/fullchain.pem
}

function main() {
  issue nyarla.net
  issue nyadgets.net
  issue kalaclista.com

  chown -R www-data:www-data -R /data/cert
  systemctl reload personal
}

main
