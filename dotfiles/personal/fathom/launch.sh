#!/usr/bin/env sh

exec /app/fathom/fathom --config=/app/fathom/env --addr=127.0.0.1:10001 --hostname=analysis.nyarla.net
