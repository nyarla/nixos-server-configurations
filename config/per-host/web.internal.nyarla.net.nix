{ config, pkgs, ... }:
{
  networking.hostName = "web.internal.nyarla.net";

  nixpkgs.config.allowUnfree = true;
  nixpkgs.overlays = [
    (import ../../overlays/extended/default.nix)
    (import ../../overlays/modified/default.nix)
  ];

  system.stateVersion  = "19.03";
}
