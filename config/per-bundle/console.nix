{ config, pkgs, ... }:
{
  programs.zsh = {
    enable                    = true;
    syntaxHighlighting.enable = true;
    enableCompletion          = true;
  };

  security.sudo.enable = true;

  environment.systemPackages = with pkgs; [
    zsh fzy
    tmux tmux-up wcwidth-cjk
   
    file gnused gawk
    gnumake
    zip unzip bzip2 p7zip gnutar lzma unrar cpio
    trash-cli

    git mercurial bazaar subversion cvs
    
    keychain

    curl wget cacert

    vimHugeX
    
    lsof
  ];
}
