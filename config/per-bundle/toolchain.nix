{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    perl perlPackages.Appcpanminus
  ];
}
