{ config, pkgs }:
{
  users.users.www-data = {
    createHome    = false;
    description   = "account for data access of www";
    group         = "www-data";
    isNormalUser  = false;
  };

  users.groups.www-data = {};
}
