{ config, pkgs, ... }:
{
  users.users.console = {
    createHome   = true;
    description  = "Console User for NixOS Servers";
    group        = "users";
    extraGroups  = [
      "wheel" "www-data"
    ];
    home         = "/home/console";
    isNormalUser = true;
    shell        = pkgs.zsh;
  };
}
