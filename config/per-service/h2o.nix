{ config, pkgs }:
{
  environment.systemPackages = with pkgs; [
    h2o acme-sh openssl perl
  ];

  systemd.services.h2o = {
    enable        = true;
    description   = "h2o http/2 server";
    wantedBy      = [ "multi-user.target" ];
    after         = [ "network.target" ];
    serviceConfig = {
      Type        = "forking";
      PIDFile     = "/var/run/h2o.pid";
      ExecStart   = "${pkgs.h2o}/bin/h2o -m daemon -c /app/h2o/h2o.yml";
      ExecReload  = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
    };
    path          = with pkgs; [
      h2o openssl perl
    ];
  }; 

  systemd.services.acme-update = {
    enable        = true;
    description   = "automatic renew service for acme certificates"; 
    unitConfig    = {
      RefuseManualStart = "no";
      RefuseManualStop  = "yes";
    };
    serviceConfig = {
      Type      = "oneshot";
      ExecStart = "${pkgs.zsh}/bin/zsh /app/h2o/renew.sh";
    };
    path          = with pkgs; [
      acme-sh zsh coreutils
    ];
  };

  systemd.timers.acme-update {
    enable      = true;
    description = "trigger acme-update service on every sunday at 03:00";
    wantedBy    = [ "timer.target" "network.target" "multi-user.target" ];
    timerConfig = {
      OnCalendar = "Sun *-*-* 03:00:00";
      Persistent = "true";
    }
  }

  services.logrotate = {
    enable = true;
    config = ''
      /data/log/*/*.log {
        ifempty
        dateformat .%Y-%m-%d
        missingok
        compress
        daily
        rotate 14
        postrotate
          /run/current-system/sw/bin/systemctl reload personal
        endscript
      }
    '';
  }
}
