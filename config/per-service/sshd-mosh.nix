{ config, pkgs }:
{
  networking.firewall = {
    enable = true;
    allowedTCPPorts = [ 80 443 57092 ];
    allowedUDPPorts = [ 80 443 ]; 
  };

  service.openssh = {
    enable          = true;
    ports           = [ 57092 ];
    listenAddresses = [
      { addr = "0.0.0.0"; port = 57092; }
    ];

    passwordAuthentication = false;
    permitRootLogin = "no";
  };

  programs.mosh.enable = true;
}

