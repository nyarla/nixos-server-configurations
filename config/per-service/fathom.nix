{ config, pkgs }:
{
  systemd.services.fathom = {
    enable        = true;
    description   = "web access analysis service by fathom";
    wantedBy      = [ "multi-user.target" ];
    after         = [ "network.target" ];
    serviceConfig = {
      Type      = "simple";
      ExecStart = "${pkgs.zsh}/bin/zsh /app/fathom/launch.sh";
      User      = "www-data";
      Group     = "www-data";
    };
  }
}
