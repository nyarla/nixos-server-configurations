{ config, pkgs, ... }:
{
  # bootloader
  # ----------

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda";

  # networking
  # ----------

  networking.dhcpcd.enable = true;

  # vnc console
  # -----------
  
  i18n.consoleFont   = "Lat2-Terminus16";
  i18n.consoleKeyMap = "us";
  i18n.defaultLocale = "en_US.UTF-8";
}
