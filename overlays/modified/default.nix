self: super: let
  require = path: args: super.callPackage (import path) args ;
in {
  tmux = super.tmux.overrideAttrs (old: rec {
    patches = [
      (super.fetchurl {
        url     = "https://raw.githubusercontent.com/z80oolong/tmux-eaw-fix/master/tmux-2.9a-fix.diff";
        sha256  = "11siyirp9wlyqq9ga4iwxw6cb4zg5n58jklgab2dkp469wxbx2ql";
      })
    ];
  });
}
