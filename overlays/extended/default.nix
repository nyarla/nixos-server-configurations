self: super: let
  require = path: args: super.callPackage (import path) args;
in {
  tmux-up     = require ./pkgs/tmux-up/default.nix { };
  wcwidth-cjk = require ./pkgs/wcwidth-cjk/default.nix { };
}
